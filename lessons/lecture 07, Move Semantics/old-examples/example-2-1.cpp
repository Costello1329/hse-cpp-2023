#include <iostream>

template <typename vt>
vt&& forward(std::remove_reference_t<vt>& obj) {
    /// Just to debug:
    std::cout << "l -> " << (std::is_lvalue_reference_v<vt&&> ? "l" : "r") << std::endl;

    return static_cast<vt&&>(obj);
}

template <typename vt>
vt&& forward(std::remove_reference_t<vt>&& obj) {
    /// Just to debug:
    std::cout << "r -> " << (std::is_lvalue_reference_v<vt&&> ? "l" : "r") << std::endl;

    static_assert(!std::is_lvalue_reference_v<vt&&>);
    return static_cast<vt&&>(obj);
}

int main () {
    int x = 0;

    forward<int>(x); // implicit move (be careful here!)

    forward<int&>(x); // l -> l
    forward<int&&>(0); // r -> r

    forward<int&&>(x); // l -> r (implicit move)
    forward<int&>(0); // r -> l (PROHIBITED)

    return 0;
}
