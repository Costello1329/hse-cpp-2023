#include <iostream>


template <typename vt>
struct simple_allocator {
    typedef vt value_type; /// I'm creating a new type called value_type which is the same type as vt.
    typedef std::size_t size_type;
    typedef std::ptrdiff_t difference_type;
    typedef const vt* const_pointer;
    typedef vt* pointer;
    /// Other typedefs (in std::allocator) were deprecated in C++17 and removed in C++20.

    pointer allocate (const size_type n) {
        std::cout << "Allocating " << n << " objects" << std::endl;
        return reinterpret_cast<vt*>(new char[n * sizeof(vt)]);
    }

    void deallocate (const pointer ptr, const size_t n) {
        std::cout << "Deallocating " << n << " objects" << std::endl;
        delete[] reinterpret_cast<char*>(ptr);
    }

    /// Deprecated in C++17 (in std::allocator) and removed in C++20.
    template <typename other_vt, typename... args_t>
    void construct (other_vt* ptr, const args_t... args) { new (ptr) vt(args...); }

    /// Deprecated in C++17 (in std::allocator) and removed in C++20.
    template <typename other_vt>
    void destroy (other_vt* ptr) { ptr->~vt(); }
};

struct logger {
    explicit logger (const size_t id): _id(id) { std::cout << "Constructed at " << this << std::endl; }
    ~logger () { std::cout << "Destructed at " << this << std::endl; }
    void print (const std::string &str) const { std::cout << _id << ": " << str << std::endl; }

private:
    size_t _id;
};

template <typename alloc_t>
void do_some_operations_with_allocator (alloc_t& alloc) {
    const size_t n = 2;

    typename alloc_t::pointer chunk = alloc.allocate(n);

    for (size_t i = 0; i < n; i ++)
        alloc.construct(chunk + i, i + 1);

    chunk[0].print("hello");
    chunk[1].print("bye");

    for (size_t i = 0; i < n; i ++)
        alloc.destroy(chunk + i);

    alloc.deallocate(chunk, n);
}

int main () {
    simple_allocator<logger> alloc;
    do_some_operations_with_allocator(alloc);
}
